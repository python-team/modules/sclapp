# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import sys, codecs, locale

def enableStdioEncoding(decode_stdin = True, encode_stdout = True,
  encode_stderr = True):
    '''Replace sys.stdin, sys.stdout, sys.stderr with stream readers & writers
    that encode/decode to/from the preferred character encoding.
    '''
    encoding = locale.getpreferredencoding()

    encode, decode, streamreader, streamwriter = codecs.lookup(encoding)
    streamreader.errors = 'replace'
    streamwriter.errors = 'replace'

    if decode_stdin:
        sys.stdin = streamreader(sys.stdin)
        sys.stdin.encoding = encoding
    if encode_stdout:
        sys.stdout = streamwriter(sys.stdout)
        sys.stdout.encoding = encoding
    if encode_stderr:
        sys.stderr = streamwriter(sys.stderr)
        sys.stderr.encoding = encoding

def disableStdioEncoding():
    '''Restore sys.stdin, sys.stdout, sys.stderr to their original objects.'''
    restoreStdin()
    restoreStdout()
    restoreStderr()

def restoreStdin():
    '''Restore sys.stdin to the original object that existed when
    _setStdinEncoding was called.  Return the replacement so that it can be
    reinstated afterwards.
    '''
    previous_stdin = sys.stdin
    if hasattr(sys.stdin, 'stream'):
        sys.stdin = sys.stdin.stream
    return previous_stdin

def restoreStdout():
    '''Restore sys.stdout to the original object that existed when
    _setStdoutEncoding was called.  Return the replacement so that it can be
    reinstated afterwards.
    '''
    previous_stdout = sys.stdout
    if hasattr(sys.stdout, 'stream'):
        sys.stdout = sys.stdout.stream
    return previous_stdout

def restoreStderr():
    '''Restore sys.stderr to the original object that existed when
    _setStderrEncoding was called.  Return the replacement so that it can be
    reinstated afterwards.
    '''
    previous_stderr = sys.stderr
    if hasattr(sys.stderr, 'stream'):
        sys.stderr = sys.stderr.stream
    return previous_stderr

def raw_input(q = ''):
    '''Decoded input-friendly replacement for the built-in raw_input function'''
    sys.stdout.write(q)
    sys.stdout.flush()

    result = ''
    ch = sys.stdin.read(1)
    while ch != '\n':
        result = '%s%s' % (result, ch)
        ch = sys.stdin.read(1)
    return result

def input(q = ''):
    return eval(raw_input(q))
