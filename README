=============
sclapp README
=============

----------------------------------------------
http://www.alittletooquiet.net/software/sclapp
----------------------------------------------

sclapp is a Python module that makes it easier to write well-behaved
command-line applications.

This file may be distributed under the same license as sclapp itself.


Installing
==========

Before installing from source, check if your distribution has packages
available.  It is not normally recommended that you install packages from
source in system-wide directories, unless you know what you're doing.

To build::

  ./setup.py build

To install::

  ./setup.py install

To clean up temporary files created while building or testing::

  ./setup.py clean

To clean all files, including built files that are required for installation::

  ./setup.py clean --all


Running Tests
=============

Tests can be run via setup.py::

  ./setup.py test

Specific tests can be specified on the command-line.  For instance, to only run
tests defined in module tests.background_command::

  ./setup.py test --tests tests.background_command

To only run tests defined by test case BackgroundCommandTestCase::

  ./setup.py test --tests tests.background_command.BackgroundCommandTestCase

To only run a specific test defined by that test case::

  ./setup.py test --tests tests.background_command.BackgroundCommandTestCase.test_basic_operation

Multiple identifiers can be specified using a comma-separated list.
