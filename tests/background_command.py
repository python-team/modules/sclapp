# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys, signal
from unittest import main

import sclapp

from common import (
  SclappTestCase,
  assertLogFileContains,
)
from manager import manager

class BackgroundCommandTestCase(SclappTestCase):
    @staticmethod
    def test_basic_operation():
        from sclapp import processes as s_processes
        from sclapp import debug_logging

        p = s_processes.BackgroundCommand(
          'echo',
          ['echo', 'testing'],
          stdout = debug_logging.DEBUG_LOGFILE
        )
        p.run()
        p.wait()
        assertLogFileContains('testing')
        assert p.getExitStatus() == 0

    @staticmethod
    def test_death_by_signal():
        import time

        from sclapp import processes as s_processes
        from sclapp import debug_logging

        p = s_processes.BackgroundCommand(
          'yes', ['yes', 'testing'], stdout = debug_logging.DEBUG_LOGFILE)
        p.run()
        time.sleep(1)
        p.kill()
        p.wait()
        assertLogFileContains('testing')
        assert p.getExitSignal() == 2

manager.add_test_case_class(BackgroundCommandTestCase)
