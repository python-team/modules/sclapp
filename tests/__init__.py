import glob, os

def get_test_modules():
    for name in glob.glob(os.path.join(os.path.dirname(__file__), '*.py')):
        if name == '__init__':
            continue
        module = os.path.basename(name)[:-3]
        yield module

def load():
    for module in get_test_modules():
        __import__('tests', {}, {}, [module])

def run(test_names = None):
    from tests.manager import manager
    manager.run(test_names)

def main(test_names = None):
    from tests.manager import manager
    manager.main(test_names)
